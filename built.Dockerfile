FROM registry.gitlab.com/vky/openjdk17u/base

WORKDIR /jdk17
COPY  . .
RUN bash configure \
        --with-jtreg='/jtreg/build/images/jtreg' \
        --with-gtest='/googletest/' \
        --with-boot-jdk='/jdk-17.0.2/' && \
    make images