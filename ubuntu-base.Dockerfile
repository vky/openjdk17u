FROM ubuntu:22.04

WORKDIR /
RUN apt update && apt upgrade -y && \
    apt install -y --no-install-recommends \
        gcc g++ \
        diffutils findutils \
        file \
        zip unzip \
        openjdk-17-jdk \
        libx11-dev libxext-dev libxrender-dev libxrandr-dev libxtst-dev libxt-dev \
        libcups2-dev \
        libfontconfig1-dev \
        libasound2-dev \
        wget \
        git && \
    git clone -b release-1.8.1 https://github.com/google/googletest && \
    git clone https://github.com/openjdk/jtreg.git

WORKDIR /jtreg
RUN bash ./make/build.sh --jdk /usr/lib/jvm/java-17-openjdk-amd64/


# RUN git config --global --add safe.directory /jdk && \
#     bash configure --with-jtreg='/jtreg/build/images/jtreg' --with-gtest='/googletest/'

# TODO: build latest tag instead of master
