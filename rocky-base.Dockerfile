FROM rockylinux:9

WORKDIR /
RUN yum update -y && \
    yum -y install \
        gcc make gcc-g++ autoconf \
        zip unzip \
        file which \
        diffutils \
        findutils \
        java-17-openjdk-devel \
        libXtst-devel libXt-devel libXrender-devel libXrandr-devel libXi-devel \
        cups-devel \
        fontconfig-devel \
        alsa-lib-devel \
        git \
        wget && \
    git clone -b release-1.8.1 https://github.com/google/googletest && \
    git clone https://github.com/openjdk/jtreg.git

WORKDIR /jtreg
SHELL ["bash", "-c"]
RUN bash ./make/build.sh --jdk /usr/lib/jvm/java-17-openjdk-17*/

